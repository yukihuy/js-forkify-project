import Search from './models/Search';
import Recipe from './models/Recipe';
import List from './models/List';
import Likes from './models/Likes';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as listView from './views/listView';
import * as likeView from './views/likeView';

import { elements, renderLoader, clearLoader } from './views/base';

const state = {};


const controlSearch = async () => {
    //1) Get query from view
    const query = searchView.getInput();

    if (query) {
        try {
            //2) New Search object and add a state
            state.search = new Search(query);
            //3) Prepare UI for results
            searchView.clearInput();
            searchView.clearResult();
            renderLoader(elements.searchRes);
            //4) Search for recipes
            await state.search.getResults();
            clearLoader();
            //5) Render results on UI
            searchView.renderResults(state.search.result);
        } catch (error) {
            alert('Something wrong with search...');
            clearLoader();
        }


    }
}

elements.searchForm.addEventListener('submit', e => {
    e.preventDefault();
    controlSearch();
});


elements.searchResPages.addEventListener('click', e => {
    const btn = e.target.closest('.btn-inline');

    if (btn) {
        const gotoPage = parseInt(btn.dataset.goto, 10);
        searchView.clearResult();
        searchView.renderResults(state.search.result, gotoPage);

    }

});

const controlRecipe = async () => {
    const id = window.location.hash.replace('#', '');
    if (id) {

        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        if (state.search) searchView.highlightSelected(id);

        state.recipe = new Recipe(id);

        try {

            //Get recipe data and parse ingredients
            await state.recipe.getRecipe();


            state.recipe.parseIngredients();

            //Calculate serving and time
            state.recipe.calcTime();
            state.recipe.calcServings();

            //Render recipe
            clearLoader();

            recipeView.renderRecipe(
                state.recipe,
                state.likes.isLiked(id)
            );
        



        } catch (err) {
            alert('Error processing recipe !');
        }


    }
}

//window.addEventListener('hashchange',controlRecipe);
['hashchange', 'load'].forEach(event => window.addEventListener(event, controlRecipe));

/**
 * LIST CONTROLLER
 */
//TESTING


const controlList = () => {

    //Create a new list IF there in none yet
    if (!state.list) state.list = new List();
   
    //Add each ingredients to the list
    state.recipe.ingredients.forEach(el => {
        const item = state.list.addItem(el.count, el.unit, el.ingredient);
        listView.renderItem(item);
    });

}

elements.shopping.addEventListener('click', e => {

    const id = e.target.closest('.shopping__item').dataset.itemid;

    if (e.target.matches('.shopping__delete, .shopping__delete *')) {

        state.list.deleteItem(id);

        listView.deleteItem(id);
    } else if (e.target.matches('.shopping__count-value')) {
        const value = parseFloat(e.target.value, 10);
        state.list.updateItem(id, value);
    }
});

/**
 * LIKE CONTROLLER
 */

const controlLike = () => {
    if (!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;
    if (!state.likes.isLiked(currentID)) {

        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
        );
        //Toggle the like button
        likeView.toggleLikeButton(true);
        //Add like to UI list
        likeView.renderLike(newLike);
        
    } else {
        //Remove like from the state
        state.likes.deleteLike(currentID);

        //Toggle the like button
        likeView.toggleLikeButton(false);
        //Remove like from UI list
        likeView.deleteLikes(currentID);
    }
    likeView.toggleLikeMenu(state.likes.getNumLike());

}
window.addEventListener('load',() => {
    state.likes = new Likes();

    state.likes.readStorage();

    likeView.toggleLikeMenu(state.likes.getNumLike());

    state.likes.likes.forEach(like => {likeView.renderLike(like)})

});
//Handling recipe button clicks
elements.recipe.addEventListener('click', e => {
    //matches() kiểm tra Element được chọn chưa :)
    if (e.target.matches('.btn-decrease, .btn-decrease *')) {

        if (state.recipe.servings > 1) {
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
    } else if (e.target.matches('.btn-increase, .btn-increase *')) {

        //Increase button is clicked 

        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        controlList();
    } else if (e.target.matches('.recipe__love, .recipe__love *')) {
        controlLike();
    }


});

