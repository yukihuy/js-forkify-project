export default class Likes {
    constructor() {
        this.likes = [];
    }

    addLike(id, title, author, img) {

        const like = { id, title, author, img };

        this.likes.push(like);

        this.persistData();

        return like;

    }
    deleteLike(id) {
        const index = this.likes.findIndex(el => el.id === id);
        this.likes.splice(index, 1);//Xóa 1 phần tử từ vị trí index
    }
    isLiked(id) {
        return this.likes.findIndex(el => el.id === id) !== -1;
    }
    getNumLike() {
        return this.likes.length;
    }
    persistData() {
        localStorage.setItem('likes', JSON.stringify(this.likes));
    }
    readStorage() {
        const storage = JSON.parse(localStorage.getItem('likes'));

        if(storage) this.likes = storage;
    }

}